package APR;

public class ClientData {

    Object clientData = new Object();

    private String Name;
    private String Address;
    private String Zip;
    private String City;
    private String State;
    private String Activities;

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getActivities() {
        return Activities;
    }

    public void setActivities(String activities) {
        Activities = activities;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getZip() {
        return Zip;
    }

    public void setZip(String zip) {
        Zip = zip;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }


}
