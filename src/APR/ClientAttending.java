package APR;
import java.util.*;

public class ClientAttending implements Client {

    private String response;
    Scanner scan = new Scanner(System.in);
    Scanner keyboard = new Scanner(System.in);
    ClientData CD = new ClientData();

    public ClientData clientDataM() {


        System.out.print("What is your name? ");
        response = scan.nextLine();
        CD.setName(response);

        System.out.print("What is your Address? (Street Address Only) ");
        response = scan.nextLine();
        CD.setAddress(response);

        System.out.print("What City are you from? ");
        response = scan.nextLine();
        CD.setCity(response);

        System.out.print("What State is your residency? ");
        response = scan.nextLine();
        CD.setState(response);

        System.out.print("What is your zip code? ");
        response = scan.nextLine();
        CD.setZip(response);


        return CD;

    }

    public void printClientData(){


        System.out.println("Information of the hunter: " + CD.getName());
        System.out.println(CD.getAddress());
        System.out.println(CD.getCity());
        System.out.println(CD.getState());
        System.out.println(CD.getZip() + "\n");

    }
}
