package APR;

public interface HuntingLodge {

   // public abstract void signIn();
    public abstract void location();
    public abstract void huntType();
    public abstract void equipment();
    public abstract void dog();
    public abstract void guide();

}