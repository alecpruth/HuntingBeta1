package APR;
// Java program to demonstrate working of Arrays.toString()


//import java.io.*;
import java.util.*;

public class AlecsLodge extends Dog implements HuntingLodge {


        public void location(){

            String locationOfHunting;
            locationOfHunting = "Kiowa";

            System.out.println("Location of the hunt: " + locationOfHunting);


        }
        public void huntType(){

            String typeOfHunt;
            typeOfHunt = "pheasant";

            System.out.println("Animal Hunting: " + typeOfHunt);

        }
        public void equipment(){

            String[] equipmentUsed = {"Shotgun", "Vest", "Ammo", "Hat"};

            System.out.println("Equipment needed: " + Arrays.toString(equipmentUsed));

        }
        public void dog(){

            String dogUsed = "Golden Retriever";
            String dogName = "Charlie";

            System.out.println("Guide Dog: " + dogUsed + "\nName of Dog: " + dogName);

        }
        public void guide(){

            String guideName = "Larry";

            System.out.println("Name of the guide: " + guideName);

        }

}
